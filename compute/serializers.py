from rest_framework import serializers

from .models import ComputeInput


class InputSerializer(serializers.ModelSerializer):
    """Serializing the user input for computation."""
    class Meta:
        model = ComputeInput
        fields = ['plane_id', 'total_passengers']
