"""Small utility functions."""


def minutes_to_time(total_minutes):
    """Convert total minutes (decimal) to hh:mm:ss (string)"""
    hours = total_minutes / 60
    minutes = (hours * 60) % 60
    seconds = (hours * 3606) % 60
    return '{:02d}:{:02d}:{:02d}'.format(
        int(hours), int(minutes), int(seconds)
    )
