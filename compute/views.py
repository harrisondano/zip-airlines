from rest_framework.decorators import api_view
from rest_framework.response import Response

from .serializers import InputSerializer
from .utils import minutes_to_time

FUEL_INIT = 200  # in liters
CONSUMPTION_RATE = 0.8
PASSENGER_CONSUMPTION_ADDEND = 0.002


@api_view(['POST'])
def fuel_consumption(request):
    """
    Computes fueld consumption rate and maximum flight duration.
    Logic:
        - The company is assessing 10 different airplanes.
        - Each airplane has a fuel tank of (200 liters * id of the airplane)
        capacity. For example, if
        the airplane id = 2, the fuel tank capacity is 2*200 = 400 liters.
        - The airplane fuel consumption per minute is the logarithm of the
        airplane id multiplied by
        0.80 liters.
        - Each passenger will increase fuel consumption for additional 0.002
        liters per minute.
    """
    # process input data
    try:
        serialized = InputSerializer(request.data, many=True).data[0]
    except KeyError:
        return Response({
            'error': ' '.join([
                'The following fields are required:',
                '`plane_id` and `total_passengers`'
                ])
            })
    except ValueError:
        return Response({
            'error': '`id` and `total_passengers` should be numeric'
            })
    plane_id = serialized['plane_id']
    total_passengers = serialized['total_passengers']
    # compute
    total_fuel = plane_id * FUEL_INIT
    fuel_consumption_rate = (plane_id * CONSUMPTION_RATE) + (
        total_passengers * PASSENGER_CONSUMPTION_ADDEND
        )
    max_flight_duration = total_fuel / fuel_consumption_rate
    # construct response data
    result = {
        # Total airplane fueld consumption per minute
        'fuel_consumption_rate': fuel_consumption_rate,
        # Maximum minutes airplane able to fly
        'max_flight_duration':  max_flight_duration,
        'max_flight_duration_time':  minutes_to_time(max_flight_duration),
    }
    return Response(result)
