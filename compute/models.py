from django.db import models


class ComputeInput(models.Model):
    """For input data. Not connected to any database table."""
    plane_id = models.IntegerField()
    total_passengers = models.IntegerField()

    class Meta:
        managed = False
