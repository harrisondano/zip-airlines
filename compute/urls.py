from django.urls import path

from . import views

urlpatterns = [
    path('fuel-consumption/', views.fuel_consumption, name='fuel_consumption'),
]
