"""zipairlines URL Configuration"""

from django.urls import path, include
from django.contrib import admin
from django.contrib.auth.models import User
from rest_framework import routers
from compute import views


urlpatterns = [
    # path('', include(router.urls)),
    path('compute/', include('compute.urls')),
    path('api-auth/', include(
        'rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
]
