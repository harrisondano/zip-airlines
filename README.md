# ZipArlines #

This is a demo Django + Django Rest Framework app for aircraft passenger capacity issue.
Coded by Harrison Dano (harrisondano@gmail.com)

## Setup ##

Create environment and install packages. Make sure to `deactivate` if you are in other virtual environment.


```bash
rm -rf .env
virtualenv -p python3 .env && source .env/bin/activate
make install_requirements
```

Create a superuser


```bash
python manage.py createsuperuser --email admin@example.com --username admin
```

Migrate and run the app

```bash
make migrate run
```

## Computation Logic and Sample ##

* The company is assessing 10 different airplanes.
* Each airplane has a fuel tank of (200 liters * id of the airplane) capacity. For example, if
the airplane id = 2, the fuel tank capacity is 2*200 = 400 liters.
* The airplane fuel consumption per minute is the logarithm of the airplane id multiplied by
0.80 liters.
* Each passenger will increase fuel consumption for additional 0.002 liters per minute.

## Compute Fuel Consumption ##


Send POST json request to `/compute/fuel-consumption` with the following request data:


```json
[{
    "plane_id": <numeric>,
    "total_passengers": <numeric>
}]

```

It will give you the following response data:

```json
{
    /* Total airplane fueld consumption per minute */
    "fuel_consumption_rate": <float>,
    /* Maximum minutes airplane able to fly */
    "max_flight_duration": <float>,
    /* Converted minutes duration to hh:mm:ss */
    "max_flight_duration_time": <str:"hh:mm:ss">
}
```

If there is an error, you will receive this response data:

```json
{
    "error": <str:error-message>
}
```

Sample errors:

* The following fields are required: `plane_id` and `total_passengers`
* `id` and `total_passengers` should be numeric